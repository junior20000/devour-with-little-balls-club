# DEVOUR With The Little Ball's Club
> Now you can burn her together with 69 pals! Nice!

![Latest Release](https://badgen.net/gitlab/release/junior20000/devour-with-little-balls-club "Latest Release")
![DEVOUR Steam Store Page](https://img.shields.io/badge/Steam-1274570?style=for-the-badge&logo=steam&logoColor=white)

<div align="center">
<img src="docs/images/logo.png" alt="Logo" style="height: 258px; width:500px;"/>
</div>

Do you have more than four friends, but you all want to play together? That is what took me to mod the game again and increase the max players possible.

## PROJECT MIGRATION
I did not see that the .dll was so huge! 40MB will add up pretty fast.

So, I will archive this repo (just for historical purposes) and the project will continue **[here](https://gitlab.com/junior20000/devour-little-balls-club)**.

## What is DEVOUR
**DEVOUR** is a coop horror game. You can find it at [Steam](https://store.steampowered.com/app/1274570/DEVOUR/).

## Installation
1. Open the game folder.
2. Rename the `GameAssembly.dll` to `GameAssembly.dll.old` (for security reasons only).
3. Extract the modified `GameAssembly.dll`.
4. Enjoy!

## Visuals
<div align="center">
<img src="docs/images/splash_art.png" alt="Splash art" style="height: 896px; width:800px;"/>
<figcaption>Fig.1 - Mod splash art</figcaption>
<br>
<img src="docs/images/le_proof.png" alt="Room 1/69 screenshot" style="height: 49px; width:800px;"/>
<figcaption>Fig.2 - Room 1/69</figcaption>
</div>

## Usage
Just host a server as you would typically do.

## Support
Sharing the mod is more than enough. As I said, I just made this mod to play with all of my friends.

As for the devs., if you are reading this, please, don't mess with the mod. We all like your game and had even more fun playing it together.

## Roadmap
- [x] Increase maximum player capacity.
- [ ] Customizable maximum player capacity.
- [ ] Custom splash art.

## Contributing
This project is not open for contribution (mainly for possible legal concerns). You may fork this repo and share your findings, tweaks, etc.
In addition, I do not want to spend too much time with it, considering that I do not know if there is a public for this mod.

## Meet the Club
- [Roberto "Bets" Schiavelli Júnior](https://www.linkedin.com/in/roberto-schiavelli-j%C3%BAnior-86a3561a9/) - Author
- [Paulo "Paus" Paiva](https://twitter.com/plyadxd) - Web Artist

## Disclaimer
- I DID NOT TEST IT WITH 69 (NICE!) PLAYERS. FOR NOW, I JUST TESTED WITH 5 PLAYERS, AND IT IS WORKING GREAT!
- I DO NOT OWN THE GAME'S CODE. THIS PROJECT IS JUST A MOD.
- I DO NOT TAKE ANY RESPONSIBILITY FOR THE ACTIONS THAT THE DEV OR STEAM MAY TAKE DUE TO THIS MOD (E.G., GAME BAN, VAC BAN, ETC.) OR SIDE EFFECTS THAT MAY AFFECT YOUR SAVE FILE, COMPUTER, DATA, ETC.

***All things considered, use this mod on your account and risk.***